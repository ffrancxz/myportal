<?php session_start(); //error_reporting(0);
		
	class connection {
				
			// public $basic_info = array('host'=>'166.62.101.81','user'=>'ic4c_usr','pwd'=>'Davinci88!','db'=>'ic4c');
			public $basic_info = array('host'=>'127.0.0.1','user'=>'root','pwd'=>'','db'=>'ic4c');
				
			function _connect ( ) {
					
					$info = $this->basic_info;
					$connection = new mysqli($info['host'],$info['user'],$info['pwd']);
					$this->_set_connection($connection);
				}
			
			function _set_connection ( $connection ) {
					
					if( isset($connection) ) {
							$_SESSION['connection'] = $connection;
						}
				}
				
			function _get_connection ( ) {
					
					if( isset( $_SESSION[ 'connection' ] ) ) {
							return $_SESSION['connection'];
						}
					return false;
				}
				
			function _disconnect () {
					
					$_SESSION['connection']->close();
				}
		}
		
	class main_query {
	
			public function __construct ( $params ) {
					
					$this->connection =  new connection;
				}
			
			function _set ( $query_string, $type=null ) {
					
					$this->connection->_connect();
					$this->connection->_get_connection()->query($query_string);
					 
					if ( $type == 1 ) {
						
							return $this->connection->_get_connection()->insert_id;
						}				
				}
				
			function _get ( $query_string ) {
					
					$this->connection->_connect();
					$query = $this->connection->_get_connection()->query($query_string);
					
					$data_array = array();
					if( mysqli_num_rows ( $query ) >= 1 ) {
						while( $row = mysqli_fetch_array( $query ) ) {
						
								array_push($data_array,$row);
							}
							
						return $data_array;
					}
				}
				
			function _advance_key_array ( $array ) {
					
					$final_array = array();
					
						for( $x=0; $x < count ( $array ) ; $x++ ) {
						
							$tmp_array = array();
							
							foreach( $array[$x] as $key=> $val ) {
							
								if( !is_integer ( $key ) ) {
								
										$tmp_array[$key] = $val;
									}
							}
							
							array_push( $final_array, $tmp_array );
						}
					
					return $final_array;
				}
			
			function _multi_set ( $query_string, $type=null ) {
					
					$this->connection->_connect();
					$query = $this->connection->_get_connection()->mysqli_multi_query($query_string);
					
					if ( $type == 1 ) {
						
							return $this->connection->_get_connection()->insert_id;
						}
				}
		}
		
	class main_output {
			
			function _json ( $array_params ) {
					
					$connection =  new connection;
					if($connection->_get_connection() == false ) {
							
							$connection->_disconnect();
						}
					if ( is_array( $array_params ) ) {
					
							exit( stripslashes ( json_encode ( $array_params,true ) ) );
						}
					else{
							exit( $array_params );
						}
				}
		}
		
	class _response {
	
		function error_response ( $array_params ) {
		
				$main_output = new main_output;
				
				switch ( $array_params[ 'type' ] ) {
					
						case "undefined-type":
						
							$array_response = array('responsecode'=>-1,'status'=>'error','responsestring'=>'ERROR: Undefined action type!');
						
						break;
						
						case "undefined-search_by":
						
							$array_response = array('responsecode'=>-1,'status'=>'error','responsestring'=>'ERROR: Undefined search type!');
							
						break;
						
						case "login-not-ok":
						
							$array_response = array('responsecode'=>-1,'class_status'=>'fa fa-remove text-red','responsestring'=>'ERROR: Invalid Login Credentails!');
							
						break;
					}
					
				$main_output->_json( $array_response );
			}
		
		function success_response ( $array_params ) {
				
				$main_output = new main_output;
				
				switch ( $array_params['type'] ) {
						
						case "get-list-ok":
							
							$array_response = $array_params['details'];
							
						break;
						
						case "login-ok":
							
							$array_response = array('responsecode'=>1,'class_status'=>'fa fa-check text-green','responsestring'=>'SUCCESS: Login successful');
						
						break;
					}
					
				$main_output->_json( $array_response );
			}
		
	}
	
	class session {
			
			function _check_if_logged_in ( ) {
					
					$curr_page = $_SERVER['REQUEST_URI'];

					if ( isset ( $_SESSION['myportal_logged_in'] ) ) {
							
							if( $curr_page != '/Myportalic4c/p_main/main-splash.php' ) {
							
									header("Location: main-splash.php");
									exit();
								}
						}
					else{
			
							if( $curr_page != '/Myportalic4c/p_main/login.php' ) {
							
									header("Location: login.php");
									exit();
								}
						}
				}
			
			function _set_session ( $sess_key, $sess_var ) {
					
					$_SESSION[$sess_key] = $sess_var;
				}
			
			function _get_session ( $sess_key ) {
					
					if ( isset ( $_SESSION[$sess_key] ) ) {
							
							return $_SESSION[$sess_key];
						}
					
					return false;
				}
		}
	
	class md5_new {
	
		protected static $salt = "m7p0rT4L@@!";
		
		private static function _sha1( $val ) {
				return sha1( $val );
			}
		
		public static function _adv( $val ) {
		
				$salt = self::$salt; //GET SALT
				$new_md5 = self::_sha1( md5 ( $val.$salt ) ); // CONCAT WITH SALT THEN MD5 Then SHA1
				return $new_md5;
			}
	}
	
	function write_to_txt_file($array_params)
{
	$old_data = file_get_contents("../../debug/debugger.txt");
	$new_data = $array_params;
	$data_to_write = $old_data."\n\n".$new_data;
	$myfile = fopen("../../debug/debugger.txt", "wb") or die("Unable to open file!");
	fwrite($myfile, $data_to_write);
	fclose($myfile);
}
?>