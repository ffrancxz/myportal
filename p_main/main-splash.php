<!doctype html>
<?php
	include("../p_classes/includes.php");
	include("../p_classes/main.php");
	
	$session = new session;
	$session->_check_if_logged_in();
?>
	<head>
		<script src="../../_plugin/jquery.js"></script>
		<script src="../p_js/my_action.js"></script>
		
<?php
	echo _includes_header();
?>
		<title>iC4C Portal - Main</title>
	</head>
	
	<body class="skin-black-light fixed">
		<header class="main-header" data-id = "<?php echo base64_encode($_SESSION['myportal_logged_in'][0]['MP_ID']) ?>">
			<a href="javascript:void(0)" class="logo">
				<b>iCan Manage</b>
			</a>
			<nav class="navbar navbar-static-top">
				<div class="container-fluid">

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="javascript:void(0)">User : <b><?php echo $_SESSION['myportal_logged_in'][0]['MP_Loginid'] ?></b></a></li>
							<li><a href="javascript:void(0)">Team Leader : </a></li>
							<li><a href="javascript:void(0)">Department : <b><?php echo $_SESSION['myportal_logged_in'][0]['MP_Department'] ?></b></a></li>
						</ul>
				  
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown notifications-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-bell-o"></i>
									<span class="label label-danger">10</span>
								</a>
								<ul class="dropdown-menu">
									<li class="header">You have 10 notifications</li>
									<li>
									<!-- inner menu: contains the actual data -->
										<div style="position: relative; overflow: hidden; width: auto; height: 200px;" class="slimScrollDiv"><ul style="overflow: hidden; width: 100%; height: 200px;" class="menu">
									<li>
										<a href="#">
											<i class="fa fa-users text-aqua"></i> 5 new members joined today
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-users text-red"></i> 5 new members joined
										</a>
									</li>

									<li>
										<a href="#">
											<i class="fa fa-shopping-cart text-green"></i> 25 sales made
									</a>
							</li>
							<li>
							<a href="#">
							<i class="fa fa-user text-red"></i> You changed your username
							</a>
							</li>
							</ul><div style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;" class="slimScrollBar"></div><div style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div>
							</li>
							<li class="footer"><a href="#">View all</a></li>
							</ul>
							</li>
							<li class="dropdown">
								<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Menu <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="javascript:void(0)">Help</a></li>
										<li><a href="javascript:void(0)">Feedback/Suggestion</a></li>
										<li class="divider"></li>
										<li><a href="logout.php">Logout</a></li>
									</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

	<div class="main-sidebar">

		<div class="sidebar">
			<ul class="sidebar-menu">
				<li class="divider"> </li>
				<li class="treeview launch_icm">
				<a href="javascript:void(0)"><i class="fa fa-rocket"></i><span>Launch ICM</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li class="btn-consumer" name="Consumer ICM"><a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Consumer</a></li>
						<li class="btn-ican-merchant" name="iCan Merchant ICM"><a href="javascript:void(0)"><i class="fa fa-circle-o"></i>iCan Merchant</a></li>
						<li class="btn-nm-merchant" name="Non Member Merchant ICM"><a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Non Member Merchant</a></li>
					</ul>
				</li>
				<li class="my_notes"><a href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i><span>My Notes</span></a></li>
				<li class="my_team"><a href="javascript:void(0)"><i class="fa fa-users"></i><span>My Team</span></a></li>
				<li class="my_stats"><a href="javascript:void(0)"><i class="fa fa-bar-chart"></i><span>My Stats</span></a></li>
				<li class="my_settings"><a href="javascript:void(0)"><i class="fa  fa-wrench"></i><span>Settings</span></a></li>
			</ul>

		</div>
	</div>
	
	<div class="content-wrapper">
		<div class="content body">

		<div class="search-holder"></div>
		<div class="row icm_details"></div>
			<div class="box main-box" style="height: 100%; min-height: 600px;">
				
				<div style="text-align: center;font-family: Verdana; font-size: 50px; color: rgba(0,0,0,0.1); font-weight: bold; line-height: 700px;">
						iCan Manage
				</div>
				
            </div>
		</div>
	</div>
	<?php
		echo _include_scripts();
	?>
	</body>
	
	<footer>
		
	</footer>

</html>
