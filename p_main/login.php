<!doctype html>
<?php
	include("../p_classes/includes.php");
	include("../p_classes/main.php");
	
	$session = new session;
	$session->_check_if_logged_in();
?>
<head>
	<script src="../../_plugin/jquery.js"></script>
	<script src="../p_js/my_account_action.js"></script>
	
<?php
	echo _includes_header();
?>
</head>
	
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="javascript:void(0)"><b>iCan</b>Manage</a>
		</div><!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>
			<form class="login-form">
				<div class="form-group has-feedback">
					<input class="form-control inploginid" placeholder="Username" type="text">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input class="form-control inppassword" placeholder="Password" type="password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="login-status"></div>
				<div class="row">
					<div class="col-xs-8"></div>
					<div class="col-xs-4">
						<button type="button" class="btn btn-primary btn-block btn-flat btn-login">Sign In</button>
					</div><!-- /.col -->
				</div>
			</form>
			<a href="javascript:void(0)">I forgot my password</a><br>
			

		</div><!-- /.login-box-body -->
	</div><!-- /.login-box -->

	<!-- jQuery 2.1.4 -->
	<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="../../plugins/iCheck/icheck.min.js"></script>
	


</body>

</html>