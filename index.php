<?php ob_start(); session_start();
	
	$curr_page = $_SERVER['REQUEST_URI'];

	if ( isset ( $_SESSION['myportal_logged_in'] ) ) {
			
			if( $curr_page != '/p_main/main-splash.php' ) {
			
					header("Location: p_main/main-splash.php");
					exit();
				}
		}
	else{

			if( $curr_page != '/p_main/login.php' ) {
			
					header("Location: p_main/login.php");
					exit();
				}
		}

ob_end_flush();?>