<?php error_reporting(0);
	
	$method = $_REQUEST;
	
	date_default_timezone_set("Asia/Manila");
	
	if ( isset( $method['type'] ) ) {
	
			include ( '../p_classes/main.php' );
			// write_to_txt_file(print_r($method,true));
			$connection = new connection;
			$connection->_connect( );
			
			$ajax =  new ajax( $method );
			$ajax->__index( );
			
			$connection->_disconnect( );
		}

class ajax {
			
		public function __construct ( $array_params ) {
				
				$this->content = $array_params;
				$this->_response = new _response;
			}
			
		function __index ( ) {
				
				$type = $this->content['type'];
				
				switch ( $type ) {
						
						case "consumer_icm":
						case "consumer_icm_more":
						
							$consumer_icm = new consumer_icm( $this->content );
							$consumer_icm->_get_list_consumer ( );
								
						break;
						
						case "ican_merchant_icm":
						case "ican_merchant_icm_more":
						
							$ican_merchant_icm = new ican_merchant_icm( $this->content );
							$ican_merchant_icm->_get_list_ican_merchant ( );
							
						break;
						
						case "nm_merchant_icm":
						case "nm_merchant_icm_more":
						
							$nm_merchant_icm = new nm_merchant_icm( $this->content );
							$nm_merchant_icm->_get_list_nm_merchant ( );
							
						break;
						
						case "consumer_icmProfile":
							
							$consumer_icm = new consumer_icm( $this->content );
							$consumer_icm->_get_consumer_profile ( );
							
						break;
						
						case "ican_merchant_icmProfile":
							
							$consumer_icm = new ican_merchant_icm( $this->content );
							$consumer_icm->_get_ican_merchant_profile ( );
							
						break;
						
						case "nm_merchant_icmProfile":
							
							$consumer_icm = new nm_merchant_icm( $this->content );
							$consumer_icm->_get_nm_merchant_profile ( );
							
						break;
						
						case "ican_merchant_icmDescriptors":
							
							$ican_merchant_icm = new ican_merchant_icm( $this->content );
							$ican_merchant_icm->_get_ican_merchant_descriptor ( );
							
						break;
						
						case "nm_merchant_icmDescriptors":
							
							$ican_merchant_icm = new nm_merchant_icm( $this->content );
							$ican_merchant_icm->_get_nm_merchant_descriptor ( );
							
						break;
						
						case "consumer_icmiCan Merchant Refund Records":
						case "consumer_icm_ican_merchant_refund_records_more":
							
							$consumer_icm = new consumer_icm( $this->content );
							$consumer_icm->_get_consumer_refund_records_ican_merchant ( );
							
						break;
						
						case "consumer_icmNon Member Merchant Refund Records":
						case "consumer_icm_nm_merchant_refund_records_more":
							
							$consumer_icm = new consumer_icm( $this->content );
							$consumer_icm->_get_consumer_refund_records_nm_merchant ( );
							
						break;
						
						case "ican_merchant_icmRefund Records":
						case "ican_merchant_icm_refund_records_more":
							
							$ican_merchant_icm = new ican_merchant_icm( $this->content );
							$ican_merchant_icm->_get_ican_merchant_refund_records ( );
							
						break;
						
						case "nm_merchant_icmRefund Records":
						case "nm_merchant_icm_refund_records_more":
							
							$nm_merchant_icm = new nm_merchant_icm( $this->content );
							$nm_merchant_icm->_get_nm_merchant_refund_records ( );
							
						break;
						
						case "get_notes":
							
							$notes = new notes( $this->content );
							$notes->_get_notes ( );
							
						break;
						
						default:
							
							$this->_response->error_response(array('type'=>'undefined-type'));
							
						break;
					}
			}
	}
	
//███████████████████████████████████████████████████████████████████████████████████████████
// FORMATTING PROCESSED DATAS FOR FRONT END USE
class _format_output {
	
	public function __construct ( $array_params ) {
			
			$this->content = $array_params;
			$this->_response = new _response;
		}
		
	function _icm_table ( $array_params ) {

			$output = 	'
								<div class="box-header">
									<h3 class="box-title">'.$array_params['box-title'].'</h3>
								</div>
						';
			
			$output .= 	'
								<div class="box-body">
									<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
										<div class="row">
											<div class="col-sm-6"></div>
											<div class="col-sm-6"></div>
										</div>
										
										<div class="row data_table_holder">
											<div class="col-sm-12 data_table" style="min-height: 450px;">
												<table id="example2" class="tablesorter table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
													<thead>
														<tr role="row">
						';
			
			foreach ( $array_params['table-header'] as $key => $val ) {
					
						$output .=		 					'<th class="sorting_asc" tabindex="0" aria-control="example2" aria-label="" aria-sort="ascending" rowspan="1" colspan="1">
																'.$val.'
															</th>';
				}
			
					$output .= 	'
														</tr>
													</thead>
													<tbody>
								';
			
			if ( count( $this->content ) >= 1 ) {
			
				for ( $x = 0 ; $x < count ( $this->content ); $x++ ) {
				

						
							if( $x % 2 == 0 ) {
									$output .= 	'		<tr role="row" class="even" ondblclick="_get._details(\''.base64_encode($this->content[$x]['key_ID']).'\',\''.$array_params['data_type'].'\',0,10)" href="javascript:void(0)">';
									
										
										foreach( $this->content[$x] as $key => $val ) {
												if ($key != 'key_ID') {
														$output .= 	'<td>'.$val.'</td>';
													}
											}
										
									
										
										
									$output .= 	'		</tr>';
								}
								
							else{
									$output .= 	'		<tr role="row" class="odd" ondblclick="_get._details(\''.base64_encode($this->content[$x]['key_ID']).'\',\''.$array_params['data_type'].'\',0,10)" href="javascript:void(0)">';
									
									foreach( $this->content[$x] as $key => $val ) {
											if ($key != 'key_ID') {
													$output .= 	'<td>'.$val.'</td>';
												}
										}
									
									$output .= 	'		</tr>';
								}	
					}
			}
			else{
								
				$output .= 	'							<tr>
															<td colspan="'.count( $array_params['table-header'] ).'" style="color: red; text-align: center;">No data Found!</td>
														</tr>';
			
			}
			
				$output .= 	'						</tbody>
													<tfoot>
														<tr>
						';
						
			foreach ( $array_params['table-header'] as $key => $val ) {
					
					$output .= 	'							<th>'.$val.'</th>';
				}
			
				$output .= 	'							</tr>
													</tfoot>
												</table>
											</div>
										</div>
												';
			
				$output .= 	'		<div>
										<div class="col-sm-5">
											<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
												Total of '.$array_params['row_count'].' entries
											</div>
										</div>
										
										<div class="col-sm-7">
											<div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
												<ul class="pagination">
													<li class="paginate_button previous" id="example2_previous">
														<a href="javascript:void(0)" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a>
													</li>
						';
			
			$page_count = ceil( $array_params['row_count'] / 10 );
			
			$start = 0;
			$end = 10;
			
			for( $x = 1; $x <= $page_count; $x++ ) {
			
					$output .= 	'					<li class="paginate_button '.$array_params['data_type'].$start.$end.'">
														<a href="javascript:void(0)" aria-controls="example2" data-dt-idx="'.$x.'" tabindex="0" onclick ="_get._icm_more_details(\''.base64_encode($this->content[$x]['key_ID']).'\',\''.$array_params['data_type'].'\',\''.$start.'\',\''.$end.'\')">
															'.$x.'
														</a>
													</li>';
					$start = $start + 10;
				}
			
				$output .= 	'						<li class="paginate_button next" id="example2_next">
														<a href="javascript:void(0)" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
						';

			return $output;
		}
	
	function _icm_table_more ( $array_params ) {
			
			$output = 						'<div class="col-sm-12 data_table" style="min-height: 450px;">
												<table id="example2" class="tablesorter table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
													<thead>
														<tr role="row">
						';
			
			foreach ( $array_params['table-header'] as $key => $val ) {
					
						$output .=		 					'<th class="sorting_asc" tabindex="0" aria-control="example2" aria-label="" aria-sort="ascending" rowspan="1" colspan="1">
																'.$val.'
															</th>';
				}
			
					$output .= 	'
														</tr>
													</thead>
													<tbody>
								';
			
			if ( count( $this->content ) >= 1 ) {
			
				for ( $x = 0 ; $x < count ( $this->content ); $x++ ) {
				

						
							if( $x % 2 == 0 ) {
									$output .= 	'		<tr role="row" class="even" ondblclick="_get._details(\''.base64_encode($this->content[$x]['key_ID']).'\',\''.$array_params['data_type'].'\',0,10)" href="javascript:void(0)">';
									
										
										foreach( $this->content[$x] as $key => $val ) {
												if ($key != 'key_ID') {
														$output .= 	'<td>'.$val.'</td>';
													}
											}
										
									
										
										
									$output .= 	'		</tr>';
								}
								
							else{
									$output .= 	'		<tr role="row" class="odd" ondblclick="_get._details(\''.base64_encode($this->content[$x]['key_ID']).'\',\''.$array_params['data_type'].'\',0,10)" href="javascript:void(0)">';
									
									foreach( $this->content[$x] as $key => $val ) {
											if ($key != 'key_ID') {
													$output .= 	'<td>'.$val.'</td>';
												}
										}
									
									$output .= 	'		</tr>';
								}	
					}
			}
			else{
								
				$output .= 	'							<tr>
															<td colspan="'.count( $array_params['table-header'] ).'" style="color: red; text-align: center;">No data Found!</td>
														</tr>';
			
			}
			
				$output .= 	'						</tbody>
													<tfoot>
														<tr>
						';
						
			foreach ( $array_params['table-header'] as $key => $val ) {
					
					$output .= 	'							<th>'.$val.'</th>';
				}
			
				$output .= 	'							</tr>
													</tfoot>
												</table>
											</div>';
											
			return $output;
		}
	
	function _icm_profile ( $array_params ) {
			
			$output = 	'							
							<div class="box-body box-profile" style="width: 80%; margin: 20px auto;">
								<img class="profile-user-img img-responsive img-circle" src="../p_images/user.png" alt="User profile picture">
								<h3 class="profile-username text-center">'.$array_params['full_name'].'</h3>
								<p class="text-muted text-center">Profile Information</p>

								<ul class="list-group list-group-unbordered">';
								
			foreach ( $this->content[0] as $key => $val ) {
					
					if( $val == '' ) {
							
							$val = 'N/A';
						}
					
					if( $key != 'key_ID' ) {
							
							$description = str_replace('_',' ',$key);
							$output .= '
										<li class="list-group-item" >
											<b>'.$description.'</b>
											<a class="pull-right">'.$val.'</a>
										</li>
									  ';
						} 
				}
			
			$output .=	'		</ul>
							</div>';
									
			return $output;
		}
		
	function _icm_descriptors ( ) {
			
			$output = 	'
							<div class="box-body box-profile" style="width: 80%; margin: 20px auto;">
								<img class="profile-user-img img-responsive img-circle" src="../p_images/globe.png" alt="User profile picture">
						';
			
			if ( $this->content[0]['Merchant_website'] != '') {
					
					$output .= 	'<h3 class="profile-username text-center">'.$this->content[0]['Merchant_website'].'</h3>';
				}
			else{
					$output .= 	'<h3 class="profile-username text-center">'.$this->content[0]['Merchant_name'].'</h3>';
				}
			
			$output .= 	'
								<p class="text-muted text-center">Descriptor(s)</p>

								<ul class="list-group list-group-unbordered">	
						';
			$y = 1;
			for ( $x = 0; $x < count ($this->content); $x++ ) {
					
					foreach ( $this->content[$x] as $key => $val ) {
							
							if( $key == 'Descriptor' ) {
							
								$description = str_replace('_',' ',$key);
								$output .= 	'
												<li class="list-group-item" >
													<b>'.$description.' '.$y.'</b>
													<a class="pull-right">'.$val.'</a>
												</li>
											';
								$y = $y+1;
							}
						}
				}
			
			$output .=	'		</ul>
							</div>';
									
			return $output;
		}
	
	function _icm_refund_records ( $array_params ) {
			
			$output = 	'	
							<div class="" style="width: 97%; margin: 0 auto;">
								<div class="box-header" style="text-align: center;">
									<h3 class="box-title">'.$array_params['table-header'].'</h3>
								</div>
								
								<div class="box-body">
									<div class="dataTables_wrapper form-inline dt-bootstrap " id="example2_wrapper ">
										<div class="row">
											<div class="col-sm-6"></div>
											<div class="col-sm-6"></div>
										</div>
										
										<div class="row data_table_holder">
											<div class="col-sm-12 data_table" style="min-height: 450px;">
												<table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable">
													<thead>
													
														<tr role="row">
						';
			
			foreach ( $array_params['thead'] as $key => $val ) {
					
					$output .= 	'
															<th aria-sort="ascending" colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">'.$val.'</th>	
								';
				}
			
			$output .= 	'
														</tr>
													</thead>
													
													<tbody>
						';
			
			if( count( $this->content ) >= 1) {
				for ( $x = 0; $x < count ( $this->content ); $x++ ) {
					
						if( $x % 2 == 0 ) {
										$output .= 	'		<tr role="row" class="even" ondblclick="" href="javascript:void(0)">';
										
											
											foreach( $this->content[$x] as $key => $val ) {
													if ($key != 'key_ID') {
															$output .= 	'<td>'.$val.'</td>';
														}
												}
											
										
											
											
										$output .= 	'		</tr>';
									}
									
								else{
										$output .= 	'		<tr role="row" class="odd" ondblclick="" href="javascript:void(0)">';
										
										foreach( $this->content[$x] as $key => $val ) {
												if ($key != 'key_ID') {
														$output .= 	'<td>'.$val.'</td>';
													}
											}
										
										$output .= 	'		</tr>';
									}
					}
			}
			else{
				
				$output .= '							<tr>
															<td colspan="'.count( $array_params['thead'] ).'" style="color: red; text-align: center;">No data Found!</td>
														</tr>';
			}
			
				
				$output .= 	'						</tbody>
													
													<tfoot>
														<tr>
						';
						
			foreach ( $array_params['thead'] as $key => $val ) {
					
					$output .= 	'							<th>'.$val.'</th>';
				}
			
				$output .= 	'							</tr>
													</tfoot>
												</table>
											</div>
										</div>
							';
			
			$output .= 	'			<div>
										<div class="col-sm-5 data_indicator">
											<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
												Total of '.$array_params['row_count'].' entries
											</div>
										</div>
										
										<div class="col-sm-7">
											<div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
												<ul class="pagination">
													<li class="paginate_button previous" id="example2_previous">
														<a href="javascript:void(0)" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a>
													</li>
						';
			
			$page_count = ceil( $array_params['row_count'] / 10 );
			
			$start = 0;
			$end = 10;
			
			for( $x = 0; $x < $page_count; $x++ ) {
					$y = $x + 1;
					$output .= 	'					<li class="paginate_button '.$array_params['data_type'].$start.$end.'">
														<a href="javascript:void(0)" aria-controls="example2" data-dt-idx="'.$y.'" tabindex="0" onclick ="_get._icm_more_details(\''.base64_encode($this->content[0]['key_ID']).'\',\''.$array_params['data_type'].'\',\''.$start.'\',\''.$end.'\')">
															'.$y.'
														</a>
													</li>';
					$start = $start + 10;
				}
			
				$output .= 	'						<li class="paginate_button next" id="example2_next">
														<a href="javascript:void(0)" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
						';

			return $output;
			
		}
	
	function _icm_refund_records_more ( $array_params ) {

			$table = 					'
											<div class="col-sm-12 icm_refund_records" style="min-height: 450px;">
												<table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable">
													<thead>
													
														<tr role="row">
						';
			
			foreach ( $array_params['thead'] as $key => $val ) {
					
					$table .= 	'
															<th aria-sort="ascending" colspan="1" rowspan="1" aria-controls="example2" tabindex="0" class="sorting">'.$val.'</th>	
								';
				}
			
			$table .= 	'
														</tr>
													</thead>
													
													<tbody>
						';
			
			if( count( $this->content ) >= 1) {
				for ( $x = 0; $x < count ( $this->content ); $x++ ) {
					
						if( $x % 2 == 0 ) {
										$table .= 	'		<tr role="row" class="even" ondblclick="" href="javascript:void(0)">';
										
											
											foreach( $this->content[$x] as $key => $val ) {
													if ($key != 'key_ID') {
															$table .= 	'<td>'.$val.'</td>';
														}
												}
											
										
											
											
										$table .= 	'		</tr>';
									}
									
								else{
										$table .= 	'		<tr role="row" class="odd" ondblclick="" href="javascript:void(0)">';
										
										foreach( $this->content[$x] as $key => $val ) {
												if ($key != 'key_ID') {
														$table .= 	'<td>'.$val.'</td>';
													}
											}
										
										$table .= 	'		</tr>';
									}
					}
			}
			else{
				
				$table .= '							<tr>
															<td colspan="'.count( $array_params['thead'] ).'" style="color: red; text-align: center;">No data Found!</td>
														</tr>';
			}
			
				
				$table .= 	'						</tbody>
													
													<tfoot>
														<tr>
						';
						
			foreach ( $array_params['thead'] as $key => $val ) {
					
					$table .= 	'							<th>'.$val.'</th>';
				}
			
				$table .= 	'							</tr>
													</tfoot>
												</table>
											</div>
										
							';
			
			return $table;
		}
	
	function format_notes ( ) {
			
			$output = 	'
							<div style="cursor: move;" class="box-header ui-sortable-handle">
								<i class="ion ion-clipboard"></i>
									<h3 class="box-title">To Do List</h3>
							</div>
							
							<div class="box-body">
								<ul class="todo-list ui-sortable">
						';
			
			for ( $x = 0; $x < count ($this->content); $x++ ) {
					
					$output .=	'
									<li>
										<span class="handle ui-sortable-handle">
											
										</span>
										
										<span class="text">
											'.$this->content[$x]['MP_Notes_Message'].'
										</span>
										
										<small class="label label-success"><i class="fa fa-clock-o"></i> '.$this->time_elapsed( time() - $this->content[$x]['MP_Notes_Elapsed_Time'] ).'</small>
										
										<div class="tools">
										<i class="fa fa-edit"></i>
										<i class="fa fa-trash-o"></i>
										</div>
									</li>
								';
				}
			
			$output .=	'
								</ul>
							</div>
						';

			return $output;
		}
	
	function time_elapsed ( $secs ) {
	
			$bit = 	array(
							' year'        => $secs / 31556926 % 12,
							' week'        => $secs / 604800 % 52,
							' day'        => $secs / 86400 % 7,
							' hour'        => $secs / 3600 % 24,
							' minute'    => $secs / 60 % 60,
						);
			   
			foreach ( $bit as $k => $v ) {
			
					if( $v > 1 ) $ret[] = $v . $k . 's';
					if( $v == 1 ) $ret[] = $v . $k;
				}
				
			array_splice( $ret, count( $ret )-1, 0, ' ' );
			
			$ret[] = 'ago.';
		   
			return join(' ', $ret);
		}
}
// FORMATTING PROCESSED DATAS FOR FRONT END USE
//███████████████████████████████████████████████████████████████████████████████████████████

//███████████████████████████████████████████████████████████████████████████████████████████
// CLASS FOR PROCESSING DATA ON CONSUMER ICM LAUNCH
class consumer_icm {

		public function __construct ( $array_params ) {
			
				$this->content = $array_params;
				$this->main_query =  new main_query('');
				$this->_response = new _response;
			}
			
		function _get_list_consumer ( ) {
				
				$search_by = $this->_format_search_by ( );
				
				$query_string = '
									SELECT 
											pre_login_id as key_ID,
											USERNAME, 
											FIRSTNAME, 
											LASTNAME, 
											EMAIL_ADDRESS 
									FROM 
											ic4c.pre_login 
									WHERE 
											'.$search_by.' 
									LIKE 
											"%'.$this->content['keyword'].'%" 
									LIMIT 
											'.$this->content['start'].',
											'.$this->content['end'].'
								';
								
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				
				$query_string = '
									SELECT COUNT(*) 
									FROM 
											ic4c.pre_login 
									WHERE 
											'.$search_by.' 
									LIKE 
											"%'.$this->content['keyword'].'%"
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output =  new _format_output( $query_data );
				
				$datas = array	(
									"table-header" 	=> array (
																"Username",
																"Firstname",
																"Lastname",
																"Email Address"
															),
									"box-title" 	=> "Consumer List",
									"start" 		=> $this->content['start'] + 1,
									"end" 			=> $this->content['start'] + count( $query_data ),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'consumer_icm'
								);
								
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_table_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_table ( $datas );
					}
				
				$this->_response->success_response(array('type'=>'get-list-ok','details'=>$display_table));
			}
			
		function _format_search_by ( ) {
				
				switch ( $this->content['search_by'] ) {
						
						case "consumer_loginid":
						
							$search_by = 'USERNAME';
							
						break;
						
						case "consumer_firstname":
							
							$search_by = 'FIRSTNAME';
							
						break;
						
						case "consumer_lastname":
						
							$search_by = 'LASTNAME';
							
						break;
						
						default:
						
							$this->_response->error_response(array('type'=>'undefined-search_by'));
							
						break;
					}
				
				return $search_by;
			}
		
		function _get_consumer_profile ( ) {
				
				$query_string = '
									SELECT 
											pre_login.pre_login_id as key_ID,
											pre_login.USERNAME as Consumer_LoginID,
											pre_login.FIRSTNAME as Firstname,
											pre_login.LASTNAME as Lastname,
											pre_login.EMAIL_ADDRESS as Email_address
									FROM
											ic4c.pre_login as pre_login
									WHERE
											pre_login.pre_login_id
									=
											"'.base64_decode($this->content['data']).'"
								';
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				
				$_format_output = new _format_output( $query_data );
				
				$datas = array	(	
									'full_name'=> ($query_data[0]['Firstname'])." ".($query_data[0]['Lastname'])
								);
				
				$display_table = $_format_output->_icm_profile ( $datas );
				
				exit($display_table);
			}
		
		function _get_consumer_refund_records_ican_merchant ( ) {
				
				$query_string = '
									SELECT
											userrequest.pre_login_id as key_ID,
											userrequest.request_id as Confirmation_number,
											userrequest.ur_descriptor as Descriptor,
											userrequest.cardno	as Card_number,
											userrequest.amount as Amount,
											userrequest.response as Refund_Status,
											userrequest.date as Date_requested,
											userrequest.email as Email_address
									FROM
											ic4c.userrequest as userrequest
									WHERE
											userrequest.pre_login_id
									=
											"'.base64_decode($this->content['data']).'"
									LIMIT 
											'.$this->content['start'].',
											'.$this->content['end'].'
								';
								
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );

				$query_string = '
									SELECT COUNT(*) 
									FROM 
											ic4c.userrequest as userrequest
									WHERE 
											userrequest.pre_login_id 
									= 
											"'.base64_decode($this->content['data']).'"
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output = new _format_output( $query_data );
				
				$datas = array 	(	
									'table-header' => 'iCan Merchant Refund Records',
									'thead' => array 	(
															'Confirmation number',
															'Descriptor',
															'Card Number',
															'Amount',
															'Refund Status',
															'Date Requested',
															'Email Address'
														),
									'start' => $this->content['start'] + 1,
									'end' => count($query_data),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'consumer_icm_ican_merchant_refund_records'				
								);
				
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_refund_records_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_refund_records ( $datas );
					}
				
				exit($display_table);
			}
			
		function _get_consumer_refund_records_nm_merchant ( ) {
				
				$query_string = '
									SELECT 
										FLOATING_CONSUMER.FC_ID as key_ID,
										FLOATING_USERREQUEST.FU_ID as Confirmation_number,
										FLOATING_MERCHANT_INFO.FMI_INFO as Descriptor,
										FLOATING_USERREQUEST.FU_CARDNO as Card_number,
										FLOATING_USERREQUEST.FU_AMOUNT as Amount,
										FLOATING_USERREQUEST.FU_REFUND_STATUS as Status,
										FLOATING_USERREQUEST.FU_DATE_REQUESTED as Date_requested,
										FLOATING_USERREQUEST.FU_EMAIL_USED_FOR_PURCHASE as Email_address
									FROM	
										ic4c.FLOATING_USERREQUEST as FLOATING_USERREQUEST
									JOIN 
										ic4c.FLOATING_MERCHANT_INFO as FLOATING_MERCHANT_INFO
									ON
										FLOATING_USERREQUEST.FU_FMI_ID
									=
										FLOATING_MERCHANT_INFO.FMI_ID
									JOIN
										ic4c.FLOATING_CONSUMER as FLOATING_CONSUMER
									ON 
										FLOATING_CONSUMER.FC_ID
									=
										FLOATING_USERREQUEST.FU_FC_ID
									WHERE 
										FLOATING_CONSUMER.FC_LOGINID = (SELECT USERNAME FROM ic4c.pre_login WHERE pre_login_id = '.base64_decode($this->content['data']).')
									AND
										FLOATING_CONSUMER.FC_PASSWORD = (SELECT PASSWORD FROM ic4c.pre_login WHERE pre_login_id = '.base64_decode($this->content['data']).')
									LIMIT
										'.$this->content['start'].',
										'.$this->content['end'].'
								';
				
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );

				$query_string = '
									SELECT COUNT(*) 
									FROM 
											ic4c.FLOATING_USERREQUEST as FLOATING_USERREQUEST
										JOIN
											ic4c.FLOATING_CONSUMER as FLOATING_CONSUMER
										ON
											FLOATING_CONSUMER.FC_ID
										=
											FLOATING_USERREQUEST.FU_FC_ID
									WHERE 
											FLOATING_CONSUMER.FC_LOGINID = (SELECT USERNAME FROM ic4c.pre_login WHERE pre_login_id = '.base64_decode($this->content['data']).')
									AND
											FLOATING_CONSUMER.FC_PASSWORD = (SELECT PASSWORD FROM ic4c.pre_login WHERE pre_login_id = '.base64_decode($this->content['data']).')
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output = new _format_output( $query_data );
				
				$datas = array 	(	
									'table-header' => 'Non Member Merchant Refund Records',
									'thead' => array 	(
															'Confirmation number',
															'Descriptor',
															'Card Number',
															'Amount',
															'Refund Status',
															'Date Requested',
															'Email Address'
														),
									'start' => $this->content['start'] + 1,
									'end' => count($query_data),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'consumer_icm_nm_merchant_refund_records'				
								);
				
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_refund_records_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_refund_records ( $datas );
					}
				
				exit($display_table);
			}
	}	
// CLASS FOR PROCESSING DATA ON CONSUMER ICM LAUNCH
//███████████████████████████████████████████████████████████████████████████████████████████

//███████████████████████████████████████████████████████████████████████████████████████████
// CLASS FOR PROCESSING DATA ON MEMBER MERCHANT ICM LAUNCH
class ican_merchant_icm {
	
		public function __construct ( $array_params ) {
				
				$this->content = $array_params;
				$this->main_query =  new main_query('');
				$this->_response = new _response;
			}
		
		function _get_list_ican_merchant ( ) {
		
				$search_by = $this->_format_search_by ( );
				
				$query_string = '
									SELECT 
											merchantid.mid as key_ID,
											merchantid.website_address,
											descriptor.descriptor_text,
											client_account.USERNAME,
											client.company 
									FROM 
											ic4c.merchantid as merchantid 
									JOIN 
											ic4c.client as client
										ON
											merchantid.clientId
										=
											client.idClient
									JOIN 
											ic4c.client_account as client_account
										ON 
											merchantid.clientId
										=
											client_account.CLIENT_ID
									JOIN
											ic4c.descriptor as descriptor
										ON
											merchantid.mid
										=
											descriptor.DESCRIPTOR_MID
									WHERE
											'.$search_by.'
									LIKE
											"%'.$this->content['keyword'].'%"
									LIMIT
											'.$this->content['start'].',
											'.$this->content['end'].'
								';
										
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );

				$query_string = '
									SELECT COUNT(*) 
									FROM 
											ic4c.merchantid as merchantid 
									JOIN 
											ic4c.client as client
										ON
											merchantid.clientId
										=
											client.idClient
									JOIN 
											ic4c.client_account as client_account
										ON 
											merchantid.clientId
										=
											client_account.CLIENT_ID
									JOIN
											ic4c.descriptor as descriptor
										ON
											merchantid.mid
										=
											descriptor.DESCRIPTOR_MID
									WHERE
											'.$search_by.'
									LIKE
											"%'.$this->content['keyword'].'%"
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output =  new _format_output( $query_data );
				
				$datas = array	(
									"table-header" 	=> array (
																"Merchant Website",
																"Descriptor",
																"Merchant Username",
																"Merchant Name"
															),
									"box-title" 	=> "iCan Merchant List",
									"start" 		=> $this->content['start'] + 1,
									"end" 			=> $this->content['start'] + count( $query_data ),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'ican_merchant_icm'
								);
								
								
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_table_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_table ( $datas );
					}
				
				$this->_response->success_response(array('type'=>'get-list-ok','details'=>$display_table));
			}
			
		function _format_search_by ( ) {
				
				switch ( $this->content['search_by'] ) {
						
						case "merchant_loginid":
							
							$search_by = 'client_account.USERNAME';
							
						break;
						
						case "merchant_descriptor":
							
							$search_by = 'descriptor.descriptor_text';
							
						break;
						
						case "merchant_website":
							
							$search_by = 'merchantid.website_address';
							
						break;
						
						case "merchant_name":
							
							$search_by = 'client.company';
							
						break;
						
						default :
							
							$this->_response->error_response(array('type'=>'undefined-search_by'));
							
						break;
					}
					
				return $search_by;
			}
			
		function _get_ican_merchant_profile ( ) {
				
				$query_string = '
									SELECT 
											merchantid.mid as key_ID,
											merchantid.website_address as Merchant_website,
											client_account.USERNAME as Merchant_LoginID,
											client.company as Merchant_name,
											client.contact_email as Email_address,
											client.contact_number as Phone_Number
									FROM 
											ic4c.merchantid as merchantid 
									JOIN 
											ic4c.client as client
										ON
											merchantid.clientId
										=
											client.idClient
									JOIN 
											ic4c.client_account as client_account
										ON 
											merchantid.clientId
										=
											client_account.CLIENT_ID
									WHERE
											merchantid.mid
									=
											"'.base64_decode($this->content['data']).'"
								';
				
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				
				$_format_output = new _format_output( $query_data );
				
				$datas = array 	(
									'full_name' => $query_data[0]['Merchant_name']
								);
				
				$display_table = $_format_output->_icm_profile ( $datas );
				
				exit($display_table);
			}
		
		function _get_ican_merchant_descriptor ( ) {
				
				$query_string = '
									SELECT
											merchantid.website_address as Merchant_website,
											descriptor.descriptor_text as Descriptor
									FROM
											ic4c.descriptor as descriptor
									JOIN 
											ic4c.merchantid as merchantid
										ON
											descriptor.DESCRIPTOR_MID
										=
											merchantid.mid
									WHERE
											descriptor.DESCRIPTOR_MID
									=
											"'.base64_decode($this->content['data']).'"
								';
				
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				
				$_format_output = new _format_output( $query_data );
				
				$display_table = $_format_output->_icm_descriptors ( );
				// write_to_txt_file(print_r($display_table,true));
				exit($display_table);
			}
			
		function _get_ican_merchant_refund_records ( ) {
				
				
				$query_string = '
									SELECT
											merchantid.mid as key_ID,
											userrequest.request_id as Confirmation_number,
											userrequest.ur_descriptor as Descriptor,
											userrequest.cardno	as Card_number,
											userrequest.amount as Amount,
											userrequest.response as Refund_Status,
											userrequest.date as Date_requested,
											userrequest.email as Email_address
									FROM
											ic4c.userrequest as userrequest
										JOIN
											ic4c.merchantid as merchantid
										ON
											merchantid.mid
										=
											userrequest.url
									WHERE
											userrequest.url
									=
											"'.base64_decode($this->content['data']).'"
									LIMIT 
											'.$this->content['start'].',
											'.$this->content['end'].'
								';
				
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
// write_to_txt_file(print_r($query,true));
				$query_string = '
									SELECT 
											COUNT(*)
									FROM
											ic4c.userrequest
									WHERE 
											url
									=
											"'.base64_decode($this->content['data']).'"
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output = new _format_output( $query_data );
				
				$datas = array 	(	
									'table-header' => 'iCan Merchant Refund Records',
									'thead' => array 	(
															'Confirmation number',
															'Descriptor',
															'Card Number',
															'Amount',
															'Refund Status',
															'Date Requested',
															'Email Address'
														),
									'start' => $this->content['start'] + 1,
									'end' => count($query_data),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'ican_merchant_icm_refund_records'				
								);
				
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_refund_records_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_refund_records ( $datas );
					}
				
				exit($display_table);
			}
	}
// CLASS FOR PROCESSING DATA ON MEMBER MERCHANT ICM LAUNCH
//███████████████████████████████████████████████████████████████████████████████████████████

	
class nm_merchant_icm {
	
		public function __construct ( $array_params ) {
			
				$this->content = $array_params;
				$this->main_query =  new main_query('');
				$this->_response = new _response;
			}
		
		function _get_list_nm_merchant ( ) {
				
				$search_by = $this->_format_search_by ( );
				
				$query_string = '
									SELECT 
											FLOATING_MERCHANT.FM_ID as key_ID,
											FLOATING_MERCHANT.FM_BUSINESS,
											FLOATING_MERCHANT.FM_WEBSITE,
											FLOATING_MERCHANT_INFO.FMI_INFO
									FROM
											ic4c.FLOATING_MERCHANT as FLOATING_MERCHANT
										JOIN	
											ic4c.FLOATING_MERCHANT_INFO as FLOATING_MERCHANT_INFO
										ON	
											FLOATING_MERCHANT.FM_ID
										=
											FLOATING_MERCHANT_INFO.FMI_FM_ID
									WHERE
											'.$search_by.'
									LIKE
											"%'.$this->content['keyword'].'%"
									LIMIT
											'.$this->content['start'].',
											'.$this->content['end'].'
								';
								
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				
				$query_string = '
									SELECT COUNT(*) 
									FROM
											ic4c.FLOATING_MERCHANT as FLOATING_MERCHANT
										JOIN	
											ic4c.FLOATING_MERCHANT_INFO as FLOATING_MERCHANT_INFO
										ON	
											FLOATING_MERCHANT.FM_ID
										=
											FLOATING_MERCHANT_INFO.FMI_FM_ID
									WHERE
											'.$search_by.'
									LIKE
											"%'.$this->content['keyword'].'%"
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output =  new _format_output( $query_data );
				
				$datas = array	(
									"table-header" 	=> array (
																"Merchant Name",
																"Merchant Website",
																"Descriptor",
															),
									"box-title" 	=> "Non Member Merchant List",
									"start" 		=> $this->content['start'] + 1,
									"end" 			=> $this->content['start'] + count( $query_data ),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'nm_merchant_icm'
								);
								
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_table_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_table ( $datas );
					}
				
				$this->_response->success_response(array('type'=>'get-list-ok','details'=>$display_table));
			}
		
		function _format_search_by ( ) {
				
				switch ( $this->content['search_by'] ) {
						
						case "merchant_descriptor":
							
							$search_by = 'FLOATING_MERCHANT_INFO.FMI_INFO';
							
						break;
						
						case "merchant_website":
							
							$search_by = 'FLOATING_MERCHANT.FM_WEBSITE';
							
						break;
						
						case "merchant_name":
							
							$search_by = 'FLOATING_MERCHANT.FM_BUSINESS';
							
						break;
						
						default :
							
							$this->_response->error_response(array('type'=>'undefined-search_by'));
							
						break;
					}
					
				return $search_by;
			}
			
		function _get_nm_merchant_profile ( ) {
				
				$query_string = '
									SELECT 
											FLOATING_MERCHANT.FM_ID as key_ID,
											FLOATING_MERCHANT.FM_BUSINESS Merchant_name,
											FLOATING_MERCHANT.FM_WEBSITE Merchant_website,
											FLOATING_MERCHANT.FM_CONTACT_EMAIL as Email_address,
											FLOATING_MERCHANT.FM_CONTACT_PHONE as Phone_Number
									FROM
											ic4c.FLOATING_MERCHANT as FLOATING_MERCHANT
									WHERE
											FLOATING_MERCHANT.FM_ID
									=
											"'.base64_decode($this->content['data']).'"
											
								';
								
				$query_data = $this->main_query->_advance_key_array( $this-> main_query->_get( $query_string ) );
				// write_to_txt_file($query_string);
				$_format_output = new _format_output( $query_data );
				
				$datas = array 	(
									'full_name'=>$query_data[0]['Merchant_name']
								);
				$display_table = $_format_output->_icm_profile ( $datas );
				
				exit($display_table);
			}
		
		function _get_nm_merchant_descriptor ( ) {
				
				$query_string = '	
									SELECT
											FLOATING_MERCHANT.FM_WEBSITE as Merchant_website,
											FLOATING_MERCHANT.FM_BUSINESS as Merchant_name,
											FLOATING_MERCHANT_INFO.FMI_INFO as Descriptor
									FROM
											ic4c.FLOATING_MERCHANT as FLOATING_MERCHANT
										JOIN
											ic4c.FLOATING_MERCHANT_INFO as FLOATING_MERCHANT_INFO
										ON
											FLOATING_MERCHANT.FM_ID
										=
											FLOATING_MERCHANT_INFO.FMI_FM_ID
									WHERE
											FLOATING_MERCHANT_INFO.FMI_FM_ID
									=
											"'.base64_decode($this->content['data']).'"
								';
				
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				
				$_format_output = new _format_output( $query_data );

				$display_table = $_format_output->_icm_descriptors ( );
				
				exit($display_table);
			}
		
		function _get_nm_merchant_refund_records ( ) {
				
				$query_string = '
									SELECT 
										FLOATING_MERCHANT.FM_ID as key_ID,
										FLOATING_USERREQUEST.FU_ID as Confirmation_number,
										FLOATING_MERCHANT_INFO.FMI_INFO as Descriptor,
										FLOATING_USERREQUEST.FU_CARDNO as Card_number,
										FLOATING_USERREQUEST.FU_AMOUNT as Amount,
										FLOATING_USERREQUEST.FU_REFUND_STATUS as Status,
										FLOATING_USERREQUEST.FU_DATE_REQUESTED as Date_requested,
										FLOATING_USERREQUEST.FU_EMAIL_USED_FOR_PURCHASE as Email_address
									FROM	
										ic4c.FLOATING_USERREQUEST as FLOATING_USERREQUEST
									JOIN 
										ic4c.FLOATING_MERCHANT_INFO as FLOATING_MERCHANT_INFO
									ON
										FLOATING_USERREQUEST.FU_FMI_ID
									=
										FLOATING_MERCHANT_INFO.FMI_ID
									JOIN 
										ic4c.FLOATING_MERCHANT as FLOATING_MERCHANT
									ON
										FLOATING_MERCHANT.FM_ID
									=
										FLOATING_USERREQUEST.FU_FM_ID
									WHERE
										FLOATING_MERCHANT.FM_ID
									=
										"'.base64_decode($this->content['data']).'"
									LIMIT
										'.$this->content['start'].',
										'.$this->content['end'].'
								';
				
				$query_data = $this->main_query->_advance_key_array( $this->main_query->_get( $query_string ) );
				// write_to_txt_file($query_string);
				$query_string = '
									SELECT COUNT(*) 
									FROM 
											ic4c.FLOATING_USERREQUEST as FLOATING_USERREQUEST
										JOIN
											ic4c.FLOATING_MERCHANT as FLOATING_MERCHANT
										ON
											FLOATING_USERREQUEST.FU_FM_ID
										=
											FLOATING_MERCHANT.FM_ID
									WHERE 
											FLOATING_MERCHANT.FM_ID
									=
											"'.base64_decode($this->content['data']).'"
								';
								
				$count = $this->main_query->_get( $query_string );
				
				$_format_output = new _format_output( $query_data );
				
				$datas = array 	(	
									'table-header' => 'Non Member Merchant Refund Records',
									'thead' => array 	(
															'Confirmation number',
															'Descriptor',
															'Card Number',
															'Amount',
															'Refund Status',
															'Date Requested',
															'Email Address'
														),
									'start' => $this->content['start'] + 1,
									'end' => count($query_data),
									"row_count" 	=> $count[0]['COUNT(*)'],
									"data_type"		=> 'nm_merchant_icm_refund_records'				
								);
				
				if( $this->content['more'] == 1 ) {
						$display_table = $_format_output->_icm_refund_records_more ( $datas );
					}
				else{
						$display_table = $_format_output->_icm_refund_records ( $datas );
					}
				
				exit($display_table);
			}
	}

class notes {
	
	public function __construct ( $array_params ) {
			
			$this->content = $array_params;
			$this->main_query = new main_query('');
		}
	
	function _get_notes (  ) {
			
			$query_string = '
								SELECT
										MNotes.MP_Case_ID as key_ID,
										MNotes.MP_Notes_Author,
										MNotes.MP_Notes_Message,
										MNotes.MP_Notes_Elapsed_Time
								FROM
										ic4c.MP_Notes as MNotes
								WHERE
										MNotes.MP_Notes_Author
								=
										"'.$_SESSION['myportal_logged_in'][0]['MP_COMID'].'"
							';
			
			$query_data = $this->main_query->_advance_key_array($this->main_query->_get($query_string));
			
			$_format_output = new _format_output( $query_data );

			$display_table = $_format_output->format_notes( );

			exit ($display_table);
		} 
}



?>