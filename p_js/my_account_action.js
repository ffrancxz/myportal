$(document).ready(function ( ) {
	
	_click._binds();
});

var _click = new function ( ) {
	
	this._binds = function ( ) {
			
			$('.btn-login').bind({
					
					click: function ( ) {
							
							_account._login();
						}
				});
			
			$('.login-form input').keyup(function(e){
			
					if(e.keyCode == 13) {
					
						_account._login();
					}
				});
		}
}
var _account = new function ( ) {
	
	this._login = function ( ) {
	
			var json_data = _get._login_vars();
			var bool = _check._login_vars ( json_data );
			
			if ( bool == true ) {
					$('.login-form input').removeAttr('style');
					callAPI( json_data, 'login-status', 'Logging in...' );
				}
			
		}
}

var _get =  new function ( ) {
	
	this._login_vars = function ( ) {
		
			var json_data = 	{
									"type":"myportal_login",
									"loginid":$('.inploginid').val(),
									"password":$('.inppassword').val()
								};
			
			return json_data;
		}
}

var _check = new function ( ) {
	
	this._login_vars = function ( array_params ) {
			
			var bool = true;
			if ( array_params['loginid'] == '' ) {
					
					$('.inploginid').css({"background":"pink"});
					bool = false;
					
				}
			if ( array_params['password'] == '' ) {
					
					$('.inppassword').css({"background":"pink"});
					bool = false;
				}
			return bool;
		}
} 

function callAPI (json_data, ajax_load_location, ajax_load_message ) {
	
	$.ajax({
			url: '../p_ajax/account_ajax.php',
			type: "POST",
			data: json_data,
			beforeSend: function ( ) {
			
					$('.'+ajax_load_location).html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i> '+ajax_load_message+'</div>');
				},
			error: function ( x, t, r ) {
					alert('error');
					if( t == 'timeout') {
					
						$('.'+ajax_load_location).html('<div class="overlay"><a class="fa fa-remove text-red">Connection timeout, Please try again!</a></div>');
					}
				},
			success: function ( response ) {
					onSUCCESS( json_data['type'], response );
				}
		});
}

function onSUCCESS ( type, response ) {

	switch( type ) {
			
			case "myportal_login":
				
				response = JSON.parse(response);
				$('.login-status').html('<div class="overlay"><a class="'+response['class_status']+'"">  '+response['responsestring']+'</a></div>');
				if( response['responsecode'] == 1 ) { 
						location.reload();
					}
				
			break;
		}
}