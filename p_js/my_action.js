$(document).ready(function(){
	
	onload._hidden();
	_click._binds();
	
});

var onload = new function () {
	
	this._hidden = function () {
			
		}
}

var _click =  new function () {
	
	this._binds =  function () {

			$('.launch_icm li').bind({
					
					click: function (){
							
							$('.icm_details').html('');
							$('.main-box').html('<div style="text-align: center;font-family: Verdana; font-size: 50px; color: rgba(0,0,0,0.1); font-weight: bold; line-height: 500px;">iCan Manage</div>');
							$('.main-box').show();
						}
				});
				
			$('.btn-consumer').bind({
				
					click: function (){
							$(this).addClass('active').siblings().removeClass('active');
							var json_data = { "default":"-Search by-","consumer_loginid":"Consumer LoginID", "consumer_firstname":"Consumer Firstname", "consumer_lastname":"Consumer Lastname" } ;
							_append._create_search_opt ( json_data );
							_append._ican_consumer();
							icm_search_type = 'consumer_icm';
						}
				});
			
			$('.btn-ican-merchant').bind({
					
					click: function () {
							$(this).addClass('active').siblings().removeClass('active');
							var json_data = { "default":"-Search by-", "merchant_loginid":"Merchant LoginID", "merchant_descriptor":"Merchant Descriptor", "merchant_website":"Merchant Website", "merchant_name":"Merchant Name"};
							_append._create_search_opt ( json_data );
							_append._ican_consumer();
							icm_search_type = 'ican_merchant_icm';
						}
				});
			
			$('.btn-nm-merchant').bind({
					
					click: function () {
							$(this).addClass('active').siblings().removeClass('active');
							var json_data = { "default":"-Search by-", "merchant_descriptor":"Merchant Descriptor", "merchant_website":"Merchant Website", "merchant_name":"Merchant Name"};
							_append._create_search_opt ( json_data );
							_append._ican_consumer();
							icm_search_type = 'nm_merchant_icm';
						}
				});
			
			$('.my_notes').bind({
					
					click: function(){
							$(this).addClass('active').siblings().removeClass('active');
							var json_data = _get._notes();
							callAPI( json_data,'main-box' );
						}
				});
				
			$('.my_team').bind({
					
					click: function(){
							$(this).addClass('active').siblings().removeClass('active');
						}
				});
		
			$('.my_stats').bind({
					
					click: function(){
							$(this).addClass('active').siblings().removeClass('active');
						}
				});
				
			$('.my_settings').bind({
					
					click: function(){
							$(this).addClass('active').siblings().removeClass('active');
						}
				});
				
			$('.icm_search_btn').bind({
					
					click: function () {
							$('.icm_details').html('');
							$('.main-box').show();
							var json_data = _get._value_icm( icm_search_type );
							callAPI( json_data,'main-box' );
						}
				});
			
			$('.icm_keyword').bind({
					
					keyup: function( ev ) { 
							
							if ( ev.which == 13 ) {
									
									$('.icm_search_btn').click();
								}
						}
				});	
		}
}

var _append = new function ( ) {
	
	this._create_search_opt = function ( array_params ) {
			search_opt = '<select class="form-control icm_search_by" style="margin-left: 20px; width: auto;">';
			$.each( array_params, function (name,value ) {
					
					if ( name == 'default' ) {
							search_opt += '<option selected style="display:none;" value="'+name+'">' + value + '</option>';
						}
					else {
							search_opt += '<option value="'+name+'">' + value + '</option>';
						}
				});
			search_opt += '</select>';
		}
		
	this._icm_main = 	[
							'<div class="input-group">',
								'<input name="q" class="form-control icm_keyword" placeholder="Search..." type="text">',
									'<span class="input-group-btn">',
										'<button name="search" id="search-btn icm_search_btn" class="btn btn-flat btn-default icm_search_btn">',
											'<i class="fa fa-search"></i>',
										'</button>',
									'</span>',
								'</input>',
							'</div></br>',
						].join("");
								
	this._ican_consumer = function () {
			$('.search-holder').html(_append._icm_main);
			$('.search-holder .input-group').append(search_opt);
			_click._binds();
		}
		
	this._icm_details = function ( array_params ) {
			
			var _display = 	[
								'<div class="col-md-3">',
									'<div class="box box-solid">',
										'<div class="box-header with-border">',
											'<h3 class="box-title">Options</h3>',
												'<div class="box-tools">',
													'<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>',
												'</div>',
										'</div>',
										
										'<div class="box-body no-padding" style="display: block;">',
											'<ul class="nav nav-pills nav-stacked">',
							].join('');
			
			$.each( array_params['left-nav'], function ( name, value ) {
			
					_display +=	[
					'<li><a href="javascript:void(0)" class = "btn' + value + '" onclick = "_get._inner_details(\''+array_params['data']+'\',\''+array_params['type']+value+'\',\''+array_params['start']+'\',\''+array_params['end']+'\')">'+value+'</a></li>'
								].join('');
				});
			
			_display +=		[
											'</ul>',
										'</div>',
									'</div>',
								'</div>',
							].join('');
			
			_display +=		[
								'<div class="col-md-9">',
									'<div class="box box-primary">',
										'<div class="box-header with-border">',
											'<h3 class="box-title">' + array_params['box-title'] + '</h3>',
												// '<div class="box-tools pull-right">',
													// '<div class="has-feedback">',
														// '<input class="form-control input-sm" placeholder="Search..." type="text">',
														// '<span class="glyphicon glyphicon-search form-control-feedback"></span>',
													// '</div>',
												// '</div>',
										'</div>',
										'<div class="row icm_details_holder" style="height: 100%; min-height: 600px;">',
											'<div style="text-align: center;font-family: Verdana; font-size: 50px; color: rgba(0,0,0,0.1); font-weight: bold; line-height: 500px;">',
												'iCan Manage',
											'</div>',
										'</div>',
							].join('');
							
			return _display;
		}
	
	this._pagination = function ( ) {
			
			var page_count = $('.pagination li').length - 2;
			var active_page = $('.pagination li.active').index();
			
			if ( active_page == 1 ) {
					$('.pagination .previous').unbind();
					$('.pagination .previous').addClass('disabled');
					$('.pagination .previous').prop('disabled',true);
				}
			else{
					$('.pagination .previous').unbind();	
					$('.pagination .previous').removeClass('disabled');
					$('.pagination .previous').prop('disabled',false);
					
					$('.pagination .previous').bind ({
					
					click : function ( ) {
							var previous_page = active_page - 1;

							$('.pagination li:eq(' + previous_page + ') a').trigger('onclick');
						}
					});
					
				}
				
			if ( active_page == page_count ) {
					$('.pagination .next').unbind();
					$('.pagination .next').addClass('disabled');
					$('.pagination .next').prop('disabled',true);
				}
			else{
					$('.pagination .next').unbind();
					$('.pagination .next').removeClass('disabled');
					$('.pagination .next').prop('disabled',false);
					
					$('.pagination .next').bind ({
					
					click : function ( ) {
							var next_page = active_page + 1;

							$('.pagination li:eq(' + next_page + ') a').trigger('onclick');
						}
					});
				}
			
			// if ( page_count > 5 ) {
					
					
				// }
		}
}
	

var _get =  new function ( ) {
	
	this._value_icm = function( icm_search_type ) {
			
			var json_data = {	
								"type":icm_search_type,
								"keyword":$('.icm_keyword').val(),
								"search_by":$('.icm_search_by').val(),
								"start":0,
								"end":10,
								"more":0
							};
			return json_data;
		}
	
	this._notes = function (  ) {
			
			var json_data = {
								"type":"get_notes",
								"data":$('.main-header').attr('data-id')
							};
			return json_data;
		}
	
	this._details = function ( array_params, type, start, end ) {
			
			$('.main-box').html('');
			
			switch ( type ) {
					
					case "consumer_icm":
						
						var data = 	{
										"left-nav"	:	[
															"Profile",
															"iCan Merchant Refund Records",
															"Non Member Merchant Refund Records",
															"Case Records"
														],
										"box-title"	:	"Consumer Details"
									};	
					break;
					
					case "ican_merchant_icm":
						
						var data = 	{
										"left-nav"	:	[
															"Profile",
															"Descriptors",
															"Refund Records",
															"Case Records"
														],
										"box-title"	:	"iCan Merchant Details"
									};
					break;
					
					case "nm_merchant_icm":
						
						var data = 	{
										"left-nav"	:	[
															"Profile",
															"Descriptors",
															"Refund Records",
															"Case Records"
														],
										"box-title"	:	"Non Member Merchant Details"
									};
					break;
				}
			
			var dataext = 	{	
								"type":type,
								"data":array_params,
								"start":start,
								"end":end
							};
			
			var finalobj = $.extend( data, dataext );
			var display = _append._icm_details(finalobj);
			
			$('.icm_details').html( display );
			
			$('.main-box').hide();
		}
		
	this._inner_details = function ( array_params, type, start, end ) {
	
			var json_data = {
								"type":type,
								"data":array_params,
								"start":start,
								"end":end,
								"more":0
							};
							alert(JSON.stringify(json_data));
			callAPI ( json_data, 'icm_details_holder' );
			
		}
	
	this._icm_more_details = function ( data, type, start, end ) {
			
			$('.'+type+start+end).addClass('active').siblings().removeClass('active');
			
			var json_data = {
								"type":type+'_more',
								"keyword":'',
								"search_by":$('.icm_search_by').val(),
								"data":data,
								"start":start,
								"end":end,
								"more":1
							};

			callAPI ( json_data, 'data_table' );
		}
			
}

function callAPI (json_data, ajax_load_location) {

	$.ajax({
			url: '../p_ajax/main_ajax.php',
			type: "POST",
			data: json_data,
			beforeSend: function ( ) {
			
					$('.'+ajax_load_location).html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
				},
			error: function ( x, t, r ) {
					alert('error');
					if( t == 'timeout') {
					
						$('.'+ajax_load_location).html('<div class="overlay"><a style="color: red;">Connection timeout, Please try again!</a></div>');
					}
				},
			success: function ( response ) {
					onSUCCESS( json_data['type'], response );
				}
		});
}

function onSUCCESS ( type, response ) {
	
	switch( type ) {
			
			case "consumer_icm":
			case "ican_merchant_icm":
			case "nm_merchant_icm":
				$('.main-box').html(response);
				if( $('.pagination li').length > 2 ) {	
						$('.pagination li:eq(1)').addClass('active').siblings().removeClass('active');
					}
				_append._pagination();
			break;
			
			case "consumer_icm_more":
			case "ican_merchant_icm_more":
			case "nm_merchant_icm_more":
				$('.data_table_holder').html(response);
				_append._pagination();
			break;
			
			case "details_consumer_icm":
			case "details_ican_merchant_icm":
			case "details_nm_merchant_icm":
				$('.icm_details').html(response);
				$('.main-box').html('');
				$('.main-box').hide();
			break;
			
			case "consumer_icmProfile":
			case "ican_merchant_icmProfile":
			case "nm_merchant_icmProfile":
				$('.icm_details_holder').html(response);
				$('.main-box').hide();
			break;
			
			case "ican_merchant_icmDescriptors":
			case "nm_merchant_icmDescriptors":
				$('.icm_details_holder').html(response);
				$('.main-box').hide();
			break;
			
			case "consumer_icmiCan Merchant Refund Records":
			case "consumer_icmNon Member Merchant Refund Records":
			case "ican_merchant_icmRefund Records":
			case "nm_merchant_icmRefund Records":
				$('.icm_details_holder').html(response);
				$('.main-box').hide();
				if( $('.pagination li').length > 2 ) {	
						$('.pagination li:eq(1)').addClass('active').siblings().removeClass('active');
					}	
				_append._pagination();
			break;
			
			case "consumer_icm_ican_merchant_refund_records_more":
			case "consumer_icm_nm_merchant_refund_records_more":
			case "ican_merchant_icm_refund_records_more":
			case "nm_merchant_icm_refund_records_more":
				$('.data_table_holder').html(response);
				_append._pagination();
			break;
			
			case "get_notes":
				$('.main-box').html(response);
			break;
		}
	
}